package ssm;

import javafx.application.Application;
import javafx.stage.Stage;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_SPAN;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME_EN;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import static ssm.StartupConstants.DEFAULT_CAPTION;
import static ssm.LanguagePropertyType.PROPERTIES_ERROR;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;



/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideShowMaker extends Application {

    // THIS WILL PERFORM SLIDESHOW READING AND WRITING

    SlideShowFileManager fileManager = new SlideShowFileManager();
    String lang = "";
    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);

    @Override
    public void start(Stage primaryStage) throws Exception {
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        //  Button EN = new Button("English");
        // Button SPAN = new Button("Español");
        HBox pane = new HBox(100);
        ComboBox languageSelect = new ComboBox();
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        Stage stage = new Stage();
        stage.setTitle("Language Selection");
        stage.setScene(scene);
           // LanguageSelect.getChildren().add(EN);
        //LanguageSelect.getChildren().add(SPAN);
        languageSelect.getItems().addAll("English", "Español");
        Label language = new Label("Language / Idioma");
        pane.getChildren().add(language);
        pane.getChildren().add(languageSelect);
       pane.getStyleClass().add("background");
        languageSelect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (languageSelect.getValue().equals("English")) {
                    UI_PROPERTIES_FILE_NAME = UI_PROPERTIES_FILE_NAME_EN;
                    stage.close();
                }

                             if(languageSelect.getValue().equals("Español")){
                            UI_PROPERTIES_FILE_NAME = UI_PROPERTIES_FILE_NAME_SPAN;
                            stage.close();
                            }
			} 
		});
       
           stage.showAndWait();
       
  
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(TITLE_WINDOW);
            primaryStage.getIcons().add(new Image("file:" + PATH_ICONS + "program_icon.png"));
            
	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WA0 A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
             PropertiesManager props = PropertiesManager.getPropertiesManager();
            String properties = props.getProperty(PROPERTIES_ERROR);
	    ErrorHandler errorHandler = ui.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, properties , "");
	    System.exit(0);
	}
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            
	    props.loadProperties(UI_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
           
            
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
         Label label = new Label("There was an error loading the properties. Please reinstall the software");
              VBox pane = new VBox();
             Scene scene = new Scene(pane);
               scene.getStylesheets().add(STYLE_SHEET_UI);
             Stage stage = new Stage();
             stage.setTitle("ERROR LOADING PROPERTIES");
             Button button = new Button("OK");
             //pane.getChildren().add();
             //pane.getChildren().add();
             pane.getChildren().add(button);
             pane.getChildren().add(label);
             pane.getStyleClass().add("background");
              button.setOnMouseClicked(e->{
                 stage.close();
             });
             stage.setScene(scene);
      
             
             stage.showAndWait();
             System.exit(0);
            return false;
        }        
    }

   
    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
}
