package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW_SELECT;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.DEFAULT_CAPTION;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.controller.ImageSelectionController;
import ssm.error.ErrorHandler;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideEditView extends HBox {
    private SlideShowMakerView slideShow;
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    boolean selected;
    private Button btSubmit;
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    String captionText=DEFAULT_CAPTION;
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
         //btSubmit = new Button("Submit");
	captionTextField = new TextField(slide.getCaption());
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
        //captionVBox.getChildren().add(btSubmit);
      
	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        captionTextField.setOnKeyReleased(e->{
            
            slide.setCaption(captionTextField.getText());
        });
	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
            
	    imageController.processSelectImage(slide, this);
	});
        
    }
    public SlideEditView getSlideEditView(){
        return this;
    }
    public Slide getSlide(){
        return slide;
    }
  public void setSlide(Slide slide){
      this.slide=slide;
  }
  public void setSelected(){
      this.selected=true;
  }
  
  public void setDeSelected(){
      this.selected=false;
  }
  public boolean isSelected(){
      if (this.selected==true){
          return true;
      }
      else{
          return false;
      }
  }
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	try {
        String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	File file = new File(imagePath);
	
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
               if(slideImage.getWidth()==0){
          PropertiesManager props = PropertiesManager.getPropertiesManager();
	String missing = props.getProperty(LanguagePropertyType.NOT_FOUND);
                               imagePath = PATH_SLIDE_SHOW_IMAGES + SLASH + DEFAULT_SLIDE_IMAGE;
                               Label label  = new Label(slide.getImageFileName() + " " + missing);
	 file = new File(imagePath);
         slide.setImagePath(PATH_SLIDE_SHOW_IMAGES);
         slide.setImageFileName(DEFAULT_SLIDE_IMAGE);
	
	    // GET AND SET THE IMAGE
         
	     fileURL = file.toURI().toURL();
	     slideImage = new Image(fileURL.toExternalForm());
             VBox pane = new VBox();
             Scene scene = new Scene(pane);
             Stage stage = new Stage();
             stage.setTitle(missing);
             Button button = new Button("OK");
             //pane.getChildren().add();
             //pane.getChildren().add();
             pane.getChildren().add(button);
             pane.getChildren().add(label);
              button.setOnMouseClicked(e->{
                 stage.close();
             });
             stage.setScene(scene);
      
             
             stage.showAndWait();
              
            }
        
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            
	    double perc = scaledWidth / slideImage.getWidth();
         
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
            ErrorHandler errorHandler = slideShow.getErrorHandler();
	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "", "");
	    System.exit(0);
	    // @todo - use Error handler to respond to missing image
	}
    }    
}