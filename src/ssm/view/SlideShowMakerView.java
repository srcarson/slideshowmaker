package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.text.Font;
import javafx.scene.Group;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javafx.scene.Scene;
import java.io.FileWriter;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.geometry.Pos;
import javafx.scene.layout.Priority;
import javafx.application.Application;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import java.nio.file.StandardOpenOption;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import properties_manager.PropertiesManager;
import properties_manager.PropertiesManager;
  import static ssm.LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.DEFAULT_SLIDE_SHOW_HEIGHT;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.SLASH;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW_SELECT;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import ssm.controller.ImageSelectionController;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;
    ImageView imageSelectionView = new ImageView();
    int slideNumber=0;
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;
    int totalSlides=0;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    Button titleButton;
    TextField TitleField;
    int count=0;
    int selected=-1;
    
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
        
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
        //primaryStage.setTitle(slideShow.getTitle());
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
        //slideEditToolbar.setSpacing(100);
       // slideEditToolbar.setAlignment(Pos.CENTER);
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	    TOOLTIP_ADD_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeSlideButton = this.initChildButton(slideEditToolbar,		ICON_REMOVE_SLIDE,	    TOOLTIP_REMOVE_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        moveSlideUpButton = this.initChildButton(slideEditToolbar,		ICON_MOVE_UP,	    TOOLTIP_MOVE_UP,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	 moveSlideDownButton = this.initChildButton(slideEditToolbar,		ICON_MOVE_DOWN,	    TOOLTIP_MOVE_DOWN,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
   
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	    fileController.handleNewSlideShowRequest();
            
             moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
	});
	loadSlideShowButton.setOnAction(e -> {
         
	    fileController.handleLoadSlideShowRequest();
               PropertiesManager props = PropertiesManager.getPropertiesManager();
                String windowTitle = props.getProperty(LanguagePropertyType.TITLE_WINDOW);
             primaryStage.setTitle(windowTitle + "-" + slideShow.getTitle());
              TitleField.setText(slideShow.getTitle());
             // fileController.handleLoadSlideShowRequest();
             moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
	});
        titleButton.setOnMouseClicked(e->{
            if(TitleField.getText().equals("")){
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
                TitleField.setText(title);
                String windowTitle = props.getProperty(LanguagePropertyType.TITLE_WINDOW);
             primaryStage.setTitle(windowTitle + "-" + TitleField.getText());
        fileController.markAsEdited();
            }
            else{
             PropertiesManager props = PropertiesManager.getPropertiesManager();
            slideShow.setTitle(TitleField.getText());
               String windowTitle = props.getProperty(LanguagePropertyType.TITLE_WINDOW);
             primaryStage.setTitle(windowTitle + "-" + TitleField.getText());
        fileController.markAsEdited();
            }
    });
          //  fileController.handleTextEditRequest();
        //});
        
        viewSlideShowButton.setOnAction(e ->{
            
            slideShow.setTitle(TitleField.getText());
            handleViewSlideShowRequest();
             moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
           
        });
   
       
	saveSlideShowButton.setOnAction(e -> {
             //slideShow.getSlides().get(0).getCaption();
             
	    fileController.handleSaveSlideShowRequest();
             PropertiesManager props = PropertiesManager.getPropertiesManager();
                String windowTitle = props.getProperty(LanguagePropertyType.TITLE_WINDOW);
             primaryStage.setTitle(windowTitle + "-" + slideShow.getTitle());
            slideShow.setTitle(TitleField.getText());
             moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
            moveSlideDownButton.setDisable(true);
	});
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
            
            fileController.markAsEdited();
	    editController.processAddSlideRequest();
            viewSlideShowButton.setDisable(false);
              slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
             moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
	});
        removeSlideButton.setOnAction(e->{
           
            fileController.markAsEdited();
            editController.processRemoveSlideRequest();
             if(slideShow.getSlides().size()==0){
            viewSlideShowButton.setDisable(true);
        }
              slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
        });
            moveSlideUpButton.setOnAction(e->{
              fileController.markAsEdited();
                editController.processmoveSlideUpRequest();
             slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
        });
               moveSlideDownButton.setOnAction(e->{
                   fileController.markAsEdited();
                editController.processmoveSlideDownRequest();
           slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
        });
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    public void handleViewSlideShowRequest() {
         
//        Image slideImage;
//	try {
//	     //GET AND SET THE IMAGE
//	   URL fileURL = file.toURI().toURL();
//	    slideImage = new Image(fileURL.toExternalForm());
//             imageSelectionView.setImage(slideImage);
//	  	} catch (Exception e) {
//       ErrorHandler errorHandler = getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
//	   // System.exit(0);
//	    // @todo - use Error handler to respond to missing image
//	}
//        
    
         
        // load the web page
        fileController.handleSaveSlideShowRequest();
        
        System.out.println("Loading website");
        
        new File("./Sites/" + slideShow.getTitle()).mkdirs();
         new File("./Sites/" + slideShow.getTitle()+ "/css").mkdirs();
         new File("./Sites/" + slideShow.getTitle()+ "/js").mkdirs();
          new File("./Sites/" + slideShow.getTitle()+ "/img").mkdirs();
          int i =0;
         while(i<slideShow.getSlides().size()){
            
          String imagePath = slideShow.getSlides().get(i).getImagePath() + SLASH + slideShow.getSlides().get(i).getImageFileName();
	File file = new File(imagePath);
          BufferedImage image = null;
        try {
          
            
            image = ImageIO.read(file);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + slideShow.getTitle()+ "/img/" + slideShow.getSlides().get(i).getImageFileName()));
            ImageIO.write(image, "gif",new File("./Sites/" + slideShow.getTitle()+ "/img/" + slideShow.getSlides().get(i).getImageFileName()));
            ImageIO.write(image, "png",new File("./Sites/" + slideShow.getTitle()+ "/img/" + slideShow.getSlides().get(i).getImageFileName()));
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
        i++;
         }
String nextPath = "./images/icons/next.png";
	File next= new File(nextPath);
String prevPath = "./images/icons/prev.png";
	File prev= new File(prevPath);
String playPath = "./images/icons/play.png";
	File play= new File(playPath);
String pausePath = "./images/icons/pause.png";
	File pause= new File(pausePath);
        BufferedImage image = null;
         try {
          
            
            image = ImageIO.read(next);
            
            
            ImageIO.write(image, "png",new File("./Sites/" + slideShow.getTitle()+ "/img/next.png"));
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
           image = null;
         try {
          
            
            image = ImageIO.read(prev);
            
            
             ImageIO.write(image, "png",new File("./Sites/" + slideShow.getTitle()+ "/img/prev.png"));
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
              image = null;
         try {
          
            
            image = ImageIO.read(play);
            
            
             ImageIO.write(image, "png",new File("./Sites/" + slideShow.getTitle()+ "/img/play.png"));
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
                     image = null;
         try {
          
            
            image = ImageIO.read(pause);
            
            
             ImageIO.write(image, "png",new File("./Sites/" + slideShow.getTitle()+ "/img/pause.png"));
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
 File html = new File( "./HTML5Application/public_html/index.html");
    
      File htmlDest = new File( "./Sites/" + slideShow.getTitle() + "/index.html");
try{
Files.copy(html.toPath(), htmlDest.toPath());
} catch(IOException e){
    
}
 File css =  new File("./HTML5Application/public_html/slideshow_style.css");
      File cssDest = new File( "./Sites/" + slideShow.getTitle() + "/css/slideshow_style.css");
try{
Files.copy(css.toPath(), cssDest.toPath());
} catch(IOException e){
    
}
    File js =  new File("./HTML5Application/public_html/Slideshow.js");

    
try{
    	String addTitle = "";
        //Specify the file name and path here
    	

    	/* This logic is to create the file if the
    	 * file is not already present
    	 */
    	

    	//Here true is to append the content to file
    	FileWriter newJs = new FileWriter(js,true);
    	//BufferedWriter writer give better performance
    	BufferedWriter appended= new BufferedWriter(newJs);
    	appended.write(addTitle);
    	//Closing BufferedWriter Stream
        appended.close();
} catch(IOException e){
    
}
      File jsDest = new File( "./Sites/" + slideShow.getTitle() + "/js/Slideshow.js");
   
     
       
try{
Files.copy(js.toPath(), jsDest.toPath());
} catch(IOException e){
    
}
File json  = new File("./data/slide_shows/" + slideShow.getTitle() + ".json");
 File jsonDest = new File( "./Sites/" + slideShow.getTitle() + "/setJson" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}

//Delete if tempFile exists
File fileTemp = new File( "./Sites/" + slideShow.getTitle() + "/setJson" + ".json");
if (fileTemp.exists()){
    fileTemp.delete();
}
 json  = new File("./data/slide_shows/" + slideShow.getTitle() + ".json");
  jsonDest = new File( "./Sites/" + slideShow.getTitle() + "/setJson" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}

       WebView browser = new WebView();
         WebEngine webEngine = browser.getEngine();
         
        webEngine.load(htmlDest.toURI().toString());
        Scene scene = new Scene(browser);
        Stage stage =  new Stage();
        stage.setScene(scene);
                stage.show();
            //new File("./Sites/" + slideShow.getTitle()+ "/img/" + file).mkdirs();
//        VBox pane = new VBox(10);
//        Scene slideShowScene = new Scene(pane); 
//           Stage slideShowStage = new Stage();
//        
//      boolean scene = true;
//       HBox arrows = new HBox();
//       Button nextButton = new Button();
//       Button prevButton = new Button();
//       
//        totalSlides=slideShow.getSlides().size();
//     //  int totalSlides= slideShow.getSlides().getLength();
//        slideShow.setSelectedSlide(slideShow.getSlides().get(slideNumber));
//      String nextArrow = "file:" + PATH_ICONS + "Next.Png";
//      String prevArrow = "file:" + PATH_ICONS + "Previous.Png";
//    
//          prevButton.setDisable(true);
//	prevButton.setGraphic(new ImageView(prevArrow));
//          nextButton.setDisable(true);
//	nextButton.setGraphic(new ImageView(nextArrow));
//        arrows.getChildren().add(prevButton);
//        arrows.getChildren().add(nextButton);
//        if(slideNumber!=0){
//            prevButton.setDisable(false);
//        }
//        if((slideNumber+1)!=totalSlides){
//           nextButton.setDisable(false);
//        }
//      
//         
//             
//             
//            
//           
//           slideShowStage.setScene(slideShowScene);  
//           slideShowStage.setTitle(slideShow.getTitle());
//          Label captionLabel = new Label(slideShow.getSelectedSlide().getCaption());
//           String imagePath = slideShow.getSelectedSlide().getImagePath() + SLASH + slideShow.getSelectedSlide().getImageFileName();
//	File file = new File(imagePath);
//        Image slideImage;
//	try {
//	     //GET AND SET THE IMAGE
//	   URL fileURL = file.toURI().toURL();
//	    slideImage = new Image(fileURL.toExternalForm());
//	  
//           imageSelectionView.setImage(slideImage);
//	    
//	    // AND RESIZE IT
//	  double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
//	    double perc = scaledHeight / slideImage.getHeight();
//	    double scaledWidth = slideImage.getWidth() * perc;
//	    imageSelectionView.setFitWidth(scaledWidth);
//	    imageSelectionView.setFitHeight(scaledHeight);
//	} catch (Exception e) {
//       ErrorHandler errorHandler = getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
//	   // System.exit(0);
//	    // @todo - use Error handler to respond to missing image
//	}
//        
//          pane.getChildren().add(imageSelectionView);
//           pane.getChildren().add(captionLabel);
//           pane.getChildren().add(arrows);
//           
//             prevButton.setOnAction((ActionEvent e) -> {
//                 
//                 slideShow.setSelectedSlide(slideShow.getSlides().get(slideNumber-1));
//                 nextButton.setDisable(true);
//            prevButton.setDisable(true);
//                captionLabel.setText(slideShow.getSelectedSlide().getCaption());
//                 System.out.println(slideShow.getSelectedSlide().getCaption());
//                 getPrevImage(slideNumber);
//                 slideNumber--;                 
//                 if((slideNumber+1)!=totalSlides){
//                     nextButton.setDisable(false);
//                 }
//                 if(slideNumber!=0){
//                     prevButton.setDisable(false);
//                 }
//                 
//      });
//         nextButton.setOnAction(e->{
//           // pane.getChildren().remove(captionLabel);
//           slideShow.setSelectedSlide(slideShow.getSlides().get(slideNumber+1));
//          captionLabel.setText(slideShow.getSelectedSlide().getCaption());
//         System.out.println(slideShow.getSelectedSlide().getCaption());
//         getNextImage(slideNumber);
//         slideNumber++;
//            nextButton.setDisable(true);
//            prevButton.setDisable(true);
//         if(slideNumber!=0){
//            prevButton.setDisable(false);
//         }
//         if((slideNumber+1)!=totalSlides){
//            nextButton.setDisable(false);
//        }
//        });
//           
//        slideShowStage.showAndWait();
//        
//        slideNumber=0;
    }
    public void getPrevImage(int slide){
        
         String imagePath = slideShow.getSlides().get(slide-1).getImagePath() + SLASH + slideShow.getSlides().get(slide-1).getImageFileName();
	File file = new File(imagePath);
        Image slideImage;
	try {
	     //GET AND SET THE IMAGE
	   URL fileURL = file.toURI().toURL();
	    slideImage = new Image(fileURL.toExternalForm());
	  
           imageSelectionView.setImage(slideImage);
	     
	    // AND RESIZE IT
	  double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
            ErrorHandler errorHandler = getErrorHandler();
    PropertiesManager props = PropertiesManager.getPropertiesManager();
	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , slideShow.getSlides().get(slide-1).getImageFileName());
	   // System.exit(0);
	    // @todo - use Error handler to respond to missing image
	}
       
        
    }
      public void getNextImage(int slide){
        
         String imagePath = slideShow.getSlides().get(slide+1).getImagePath() + SLASH + slideShow.getSlides().get(slide+1).getImageFileName();
	File file = new File(imagePath);
        Image slideImage;
	try {
	     //GET AND SET THE IMAGE
	   URL fileURL = file.toURI().toURL();
	    slideImage = new Image(fileURL.toExternalForm());
	  
           imageSelectionView.setImage(slideImage);
	     
	    // AND RESIZE IT
	  double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
               ErrorHandler errorHandler = getErrorHandler();
    PropertiesManager props = PropertiesManager.getPropertiesManager();
	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , slideShow.getSlides().get(slide+1).getImageFileName());
	    // @todo - use Error handler to respond to missing image
	}
      }
    public void updateStage(){
        
    }
    public void handlePrevButton(){
             slideNumber-=1;
         }
    public void handleNextButton(){
             slideNumber+=1;
         }
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add("horizontal");
        //fileToolbarPane.setHgap(10);
        //  fileToolbarPane.setVgap(10);
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        TitleField = new TextField(slideShow.getTitle());
        
        TitleField.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
	String title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
        titleButton = new Button(title);
        titleButton.getStyleClass().add("font");
       // titleButton.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
        fileToolbarPane.getChildren().add(TitleField);
        fileToolbarPane.getChildren().add(titleButton);
      //VBox VBoxTitle=new VBox();
    // VBoxTitle.addCaptionLabel()
       
    }
    public void handleSlideSelect(SlideEditView slide){
        
        slide.setBorder(null);
        slide.updateSlideImage();
    }
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
      
            primaryStage.setTitle(windowTitle);
        
	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
        ssmPane.getStyleClass().add("background");
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);

	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
        if(slideShow.getSlides().size()==0){
	viewSlideShowButton.setDisable(true);
        }
        else{
          viewSlideShowButton.setDisable(false);  
        }
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
        TitleField.setText(slideShow.getTitle());
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stageTitle = props.getProperty(LanguagePropertyType.TITLE_WINDOW);
        primaryStage.setTitle(stageTitle + "-" + slideShow.getTitle());
    }
    

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
        int slideNum=-1;
	slidesEditorPane.getChildren().clear();
        
        // slideShow.setSelectedSlide(null);
        
	for (Slide slide : slideShowToLoad.getSlides()) {
       
      
	    SlideEditView slideEditor = new SlideEditView(slide);
         slide.setSlideEdit(slideEditor);
            
            //Background background = slideEditor.getBackground();
            //reloadSlideShowPane(slideShowToLoad);
            slideEditor.captionTextField.setOnKeyPressed(e->{
                fileController.markAsEdited();
            });
             ImageSelectionController imageController;
            
            imageController = new ImageSelectionController();
            slideEditor.imageSelectionView.setOnMousePressed(e->{
                fileController.markAsEdited();
                
	
            
	    imageController.processSelectImage(slide, slideEditor);
             slide.getSlideEdit().getStyleClass().remove(CSS_CLASS_SLIDE_EDIT_VIEW_SELECT); 
             slide.getSlideEdit().getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW); 
               moveSlideDownButton.setDisable(true);
                moveSlideUpButton.setDisable(true);
                removeSlideButton.setDisable(true);
            });
            slideEditor.setOnMouseClicked(e->{
         
                
                moveSlideDownButton.setDisable(true);
                moveSlideUpButton.setDisable(true);
              //handleSelect(slideShowToLoad, slide);  
               
            
              
              
           
              //editController.processSelectRequest();
           // slideEditor.updateSlideImage();
            
             slideShow.setSelectedSlide(slide);
              handleSlideSelected();
                removeSlideButton.setDisable(false);
                if(!(slide.equals(slideShowToLoad.getSlides().get(0)))){
                    moveSlideUpButton.setDisable(false);
                }
                if(!(slide.equals(slideShowToLoad.getSlides().get(slideShowToLoad.getSlides().size()-1)))){
                    moveSlideDownButton.setDisable(false);
                }
                 
            });
          
         
               
	    slidesEditorPane.getChildren().add(slideEditor);
              
           // slideEditor.setBackground(background);
            
	}
    }
    public void handleSlideSelected(){
        Slide slide= slideShow.getSelectedSlide();
         slide.getSlideEdit().getStyleClass().remove(CSS_CLASS_SLIDE_EDIT_VIEW_SELECT); 
     slide.getSlideEdit().getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW_SELECT); 
                
                 for(int i=0; i<slideShow.getSlides().size(); i++){
                     if(!(slide.equals(slideShow.getSlides().get(i)))){
                  
                   slideShow.getSlides().get(i).getSlideEdit().getStyleClass().remove(CSS_CLASS_SLIDE_EDIT_VIEW_SELECT);
              slideShow.getSlides().get(i).getSlideEdit().getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW); 
               }
                }
   }

}
