package ssm.error;

import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.Scene; 
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import static ssm.StartupConstants.STYLE_SHEET_UI;
/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class ErrorHandler {
    // APP UI
    private SlideShowMakerView ui;
    
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(LanguagePropertyType errorType, String errorDialogTitle, String errorDialogMessage)
    {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String errorFeedbackText = props.getProperty(errorType) + ": ";
        Label errorFeedBack= new Label(errorFeedbackText);
        Label errorDialog= new Label(errorDialogMessage);
             VBox pane = new VBox();
             Scene scene = new Scene(pane);
               scene.getStylesheets().add(STYLE_SHEET_UI);
             Stage stage = new Stage();
             stage.setTitle(errorDialogTitle);
             Button button = new Button("OK");
             pane.getChildren().add(errorFeedBack);
             pane.getChildren().add(errorDialog);
             pane.getChildren().add(button);
             pane.getStyleClass().add("background");
             button.setOnMouseClicked(e->{
                 stage.close();
             });
             stage.setScene(scene);
             stage.showAndWait();
         
             
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        // @todo
    }    
}
